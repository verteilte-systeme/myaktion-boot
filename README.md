The project My-Aktion implements a REST API with Spring Boot for
managing donation campaigns.
The repository contains a step-by-step implementation following
the respective exercise in the hands-on training booklet of lecture
**Distributed Systems**. You'll find the use cases and the domain model in the booklet, too.

Each part of the exercise is realized in a separate branch.
The branches were built on each other (technically speaking: they were rebased on their respective predecessor)
in the following order:

* start
* domain
* service-data
* logging
* rest-api
* validation
* advices
* extra-mile

You can checkout the branches to see the state of development for the corresponding part of the exercise
by `git checkout <branch_name>.` or by using the GitLab UI.

This `README.md` file may contain specific informations about the current branch.
