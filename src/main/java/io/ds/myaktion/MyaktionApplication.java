package io.ds.myaktion;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MyaktionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyaktionApplication.class, args);
	}

	@Bean
	CommandLineRunner initLoggerBean() {
		return (args)->{
			System.out.println("Hello World");
		};
	}
}
